<?php

require_once("WebSite.php");

class CkEditor extends WebSite
{
    private $detail;

    public function __construct()
    {
        parent::__construct();
    }
    public function __destruct()
    {
        parent::__destruct();
    }
    public function ckEditor()
    {
        $user = $_SESSION['username'];
        $editor = $this->getDetail();
        $editquery = "INSERT INTO ckeditor(username,detail) 
            VALUES(?,?) ";
        $stmt = $this->connection->stmt_init();
        if ($stmt->prepare($editquery)) {
            $stmt->bind_param("ss", $user, $editor);
            $stmt->execute();

            if (mysqli_error($this->connection)) {
                echo mysqli_error($this->connection);
            } else {
                header("Refresh:0;url=accunt.php");
            }
        }
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function setDetail($detail)
    {
        $this->detail = $detail;
    }

}

if($order == "ckeditor"){
    $messg = new CkEditor();
    $messg->setDetail($_REQUEST['detail']);
    $messg->ckEditor();
}


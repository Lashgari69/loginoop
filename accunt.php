
<?php
session_start() ;

if ($_SESSION['username']==null){
    header("Refresh:0;url=login.php");
    exit();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>سایت من</title>
    <link href="ds/css/main.css" rel="stylesheet" type="text/css">
    <link href="ds/css/styles.css" rel="stylesheet" type="text/css">
    <link href="ds/css/mediaqueries.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Crushed' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="ds/js/html5.js" type="text/javascript"></script>
    <link href="ds/css/ie.css" rel="stylesheet" type="text/css"/>
    <![endif]-->
    <script src="ds/js/jquery.js" type="text/javascript"></script>
    <script src="ds/js/main.js" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>
    <script type="text/javascript">
        function doAjax() {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    //down line for show in div and id=result
                     document.getElementById("result").innerHTML = xmlhttp.responseText;
                    //alert(xmlhttp.responseText);
                }else if(xmlhttp.status == 404){
                    document.getElementById("result").innerHTML = "page not found";
                    //alert("page not found");
                }
            };
            xmlhttp.open("GET", "ShowCk.php", true);
            xmlhttp.send();
        }
    </script>

</head>
<body>
<div class="pageWidth">
    <!-- Start Header-->
    <header id="header">
        <h1 id="logo">


        </h1>
    </header>
    <nav id="navigation" class="clearfix"><a href="#" id="menu">Menu</a>
        <ul>
            <?php
            require_once "menu";
            ?>

        </ul>
    </nav>
    <h1>
    <img src="avatar/<?php echo $_SESSION['username']; ?>-at.jpg" width="100">

    <br>
        message
        <?php
        echo $_SESSION['username'];
        ?>
    <button onclick="doAjax()">Show message </button>
    <br>
    <!-- div for show ajax in top -->
    <div id="result"></div>
    </h1>
        <div id="banner">
        <ul>
            <li><img src="ds/img/banner-img.jpg" alt=""/></li>
        </ul>
    </div>
    <!--  END Header-->
    <!-- Start Middle-->
    <section id="container">
        <article class="pageControl">
            <div class="pageContainer" id="home"><h2>به سایت ما خوش آمدید</h2>


                <form action="WebSite.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="order" value="avatar">
                    <table border="1">
                        <tr>
                            <td>
                                <strong>  عکس :    </strong>
                                <input type="file" name="avatar" id="avatar">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" value="upload">
                            </td>
                        </tr>
                    </table>
                </form>
                <br><br>
                <form action="CkEditor.php" method="post">
                    <input type="hidden" name="order" value="ckeditor">
                                <textarea id="detail" name="detail" ></textarea>
                                <script>
                                    ClassicEditor
                                        .create( document.querySelector( '#detail' ) )
                                        .catch( error => {
                                            console.error( error );
                                        } );
                                </script>
                                <input type="submit" value="send">
                </form>



            <div class="pageContainer"><h2>درباره ما</h2>



            </div>
        </article>
    </section>
    <!-- End Middle-->
    <!-- Start Footer-->
    <footer id="footer">
        <div class="copyRight">© Copyright 2014 | My Website</div>
    </footer>
    <!-- END Footer-->
</div>
</body>
</html>

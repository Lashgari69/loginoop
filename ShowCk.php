<?php

require_once("CkEditor.php");

class ShowCk extends CkEditor
{
    public function __construct()
    {
        parent::__construct();
    }
    public function __destruct()
    {
        parent::__destruct();
    }
    public function showCk()
    {
        $user = $_SESSION['username'];
        $showq = "SELECT * FROM ckeditor WHERE username=? ";
        $stmt = $this->connection->stmt_init();
        if ($stmt->prepare($showq)) {
            $stmt->bind_param("s", $user);
            $stmt->execute();
            $result = $stmt->get_result();
            if (mysqli_num_rows($result) != 0) {
                while ($list = mysqli_fetch_assoc($result)) {
                    echo "Hi\n\n" . $list['username'] . "\r\r You Have  message :\n\n";
                    echo $list['detail'];
                }
            }else{
                echo $user . "\r\r\n\nYou don't have message! ";
            }
        }
    }

}


$showck = new ShowCk;
$showck->showCk();
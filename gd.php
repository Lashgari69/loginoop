<?php

session_start();
header("Content-Type: image/png");

$im = imagecreate(110, 40);
$bg = imagecolorallocate($im, 0, 0, 26);

$pxlColor = imagecolorallocate($im, 255, 255, 0);
//$pxlColor2 = imagecolorallocate($im, 255, 255, 0);
for ($i = 0; $i <= 200; $i++) {
    imagesetpixel($im, rand(0, 100), rand(0, 50), $pxlColor);
}
/*for ($i = 0; $i <= 200; $i++) {
    imagesetpixel($im, rand(0, 100), rand(0, 50), $pxlColor2);
}*/

//lines
$LineColor = imagecolorallocate($im, 179, 134, 0);

for ($j = 0; $j < 5; $j++) {
    imageline($im, rand(5, 20), rand(5, 50), rand(30, 100), rand(20, 100), $LineColor);
}

$txtColor = imagecolorallocate($im, 255, 255, 255);

$str = randString(6);
$_SESSION['code'] = $str;

imagestring($im, 40, 10, 20, $str, $txtColor);


imagepng($im);
imagedestroy($im);


function randString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
<?php


error_reporting(10);


class WebSite
{
    private $email,$phone,$password,$avatar,$code;
    protected $connection,$username;

    public function __construct()
        {
            $this->connection = $this->connect();
            session_start();

        }

    public function __destruct()
        {
            $this->connection->close();
        }

    public function connect()
    {
        $connection = new mysqli("localhost", "root", "123456", "website1");
        mysqli_query($connection, "SET NAMES 'utf8'");
        mysqli_query($connection,'SET CHARACTER SET utf8');
        if ( $connection-> connect_error){
            die("error in connetion please try again". $connection->connect_error);
        }else{
            return $connection ;
        }
    }

    public function userExist($username)
    {
     $username = $this->getUsername();
     $selectq = "SELECT * FROM register WHERE username =?";
     $stmt = $this->connection->stmt_init();
     if ($stmt->prepare($selectq)){
         $stmt->bind_param("s",$username);
         $stmt->execute();
         $response = $stmt->get_result();
         if (mysqli_num_rows($response) == 0){
             return false;
         }else{
             return true;
         }
     }
    }

    public function register()
    {
        $username = $this->getUsername();
        $email = $this->getEmail();
        $phone = $this->getPhone();
        $password = $this->getPassword();

        if ($this->userExist($username))
        {
            header("Refresh:3;url=register.php");
            echo "user exit ! please change username";
        }else {
            $registerq = "INSERT INTO register(username,email,phone,password) VALUES(?,?,?,?) ";
            $stmt = $this->connection->stmt_init();
            if ($stmt->prepare($registerq)){
                $stmt->bind_param("ssss",$username,$email,$phone,$password);
                $stmt->execute();

                if (mysqli_error($this->connection)) {
                    echo mysqli_error($this->connection);
                } else {
                    header("Refresh:1;url=login.php");
                    echo "new user has been registered";
                    $this->logEvent("New User has been register($username $email $phone ");
                }
            }
        }
    }

    public function login()
    {
        $username = $this->getUsername();
        $password = $this->getPassword();
        $code = $this->getCode();
        $captchaString = $_SESSION['code'];
        if ($captchaString == $code){
            $loginq = "SELECT * FROM register WHERE username=? AND password =?";
            $selectq = $this->connection->stmt_init();
            if ($selectq->prepare($loginq)) {
                $selectq->bind_param("ss", $username, $password);
                $selectq->execute();
                $result = $selectq->get_result();
                if (mysqli_num_rows($result) == 0) {
                    header("Refresh:1;url=login.php");
                    $_SESSION['username'] = null;
                    echo "user/pass are wrong!!!";
                } else {
                    while ($list = mysqli_fetch_assoc($result)) {
                        header("Refresh:0;url=accunt.php");
                        $_SESSION['username'] = $list['username'];
                        $this->logEvent("User loggin: ($username)");
                    }
                }
            }
        }else{
            echo "code is wrong!!";
            header("Refresh:1;url=login.php");
        }
    }

    public function avatar(){
        $username = $_SESSION['username'];
        $avatar = "avatar/$username-at.jpg";
        $avaquery = "INSERT INTO avatar(username,user_avatar) 
        VALUES(?,?) ";
        $stmt = $this->connection->stmt_init();
        if ($stmt->prepare($avaquery)) {
            $stmt->bind_param("ss", $username, $avatar);
            $stmt->execute();
            if (mysqli_error($this->connection)) {
                echo mysqli_error($this->connection);
            } else {
                header("Refresh:0;url=accunt.php");
            }

        }
    }

    public function logEvent($msg)
    {
        date_default_timezone_set('Asia/Tehran');
        $date =date('Y-m-d H:i:s', time());
        $userIP =$_SERVER['REMOTE_ADDR'];
        file_put_contents("log",
            $date . " " .
            $userIP . " " .
            $msg . "\n",
            FILE_APPEND);
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }





}

$order = $_REQUEST['order'];

if ($order == "register"){
    $newuser = new WebSite();
    $newuser->setUsername($_REQUEST['username']);
    $newuser->setEmail(filter_var($_REQUEST['email'],FILTER_VALIDATE_EMAIL));
    $newuser->setPhone(preg_replace('/[^0-9]/', '', $_REQUEST['phone']));
    $newuser->setPassword((md5($_REQUEST['password'])));
    $newuser->register();
}
if ($order == "login"){
    $userlogin = new WebSite();
    $userlogin->setUsername($_REQUEST['username']);
    $userlogin->setPassword(md5($_REQUEST['password']));
    $userlogin->setCode($_REQUEST['code']);
    $userlogin->login();
}
if ($order =='logoff') {
    $_SESSION['username'] = null;
    header("Refresh:0;url=login.php");
}

if($order == "avatar"){
    $avatar = new WebSite();
    $username = $_SESSION['username'];
    $destination = "avatar/$username-at.jpg";
    $avatar->setAvatar(move_uploaded_file($_FILES['avatar']['tmp_name'], $destination));
    $avatar->avatar();
}



